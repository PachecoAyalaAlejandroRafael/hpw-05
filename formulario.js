function crear_form(nombre,id){
  var obj=document.createElement("form");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_text(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","text");
  obj.setAttribute("name",nombre);
  obj.setAttribute("placeholder",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_required(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","required");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_textarea(nombre,id,texto)
{
  var obj=document.createElement("botton");
  obj_text=document.createTextNode(texto);
  obj.appendChild(obj_text);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_password(nombre,id,placeholder)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","password");
  obj.setAttribute("placeholder",placeholder);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj; 
}

function crear_input_radio(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","radio");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_checkbox(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","checkbox");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_text_placeholder(nombre,idplaceholder)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","text_placeholder");
  obj.setAttribute("placeholder",placeholder);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_search(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","search");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_file(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","file");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_submit(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","submit");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_reset(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","reset");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_email(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","email");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_date(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","date");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_time(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","time");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_month(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","month");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_week(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","week");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_botton(nombre,id)
{
  var obj=document.createElement("botton");
  obj_text=document.createTextNode(nombre);
  obj.appendChild(obj_text);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_number(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","number");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_tel(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","tel");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_url(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","url");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_range(nombre,id,min,max,step)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","range");
  obj.setAttribute("min",min);
  obj.setAttribute("max",max);
  obj.setAttribute("step",step);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

opciones=[
  "opcion 1","opcion 2","opcion 3"
  ];

function crear_select(nombre,id,opcion)
{
  var obj=document.createElement("select");
  obj.setAttribute("type","select");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  for(var i=0;i<opcion.length;i++)
  {
    var op=document.createElement("option");
    op_text=document.createTextNode(opcion[i]);
    op.appendChild(op_text);
    op.setAttribute("value",opcion[i]);
    obj.appendChild(op);
  }
  return obj;
}



function div(cadena,objeto)
{
  var obj=document.createElement("div");
  obj.setAttribute("class","row");
  var ob=document.createElement("div");
  ob.setAttribute("class","col-md-4");
  var titulo=document.createElement("h1");
  var ob_text=document.createTextNode(cadena);
  ob.appendChild(ob_text);
  var ob1=document.createElement("div");
  ob1.setAttribute("class","col-md-8");
  ob1.appendChild(objeto);
  obj.appendChild(ob);
  obj.appendChild(ob1);
  return obj;
}

function crear_container()
{
  var obj=document.createElement("div");
  obj.setAttribute("class","container-fluid");
  var ob=document.createElement("legend");
  var ob_text=document.createTextNode("Formulario");
  ob.appendChild(ob_text);
  obj.appendChild(ob);
  return obj;
}


var selec=crear_select("opciones","1313",opciones);

function asociar_hijo(hijo,padre)
{
  padre.appendChild(hijo);
}



var formulario=crear_form("formulario","1010");
var texto=crear_input_text("texto1","1111");
var email=crear_input_email("email1","1414");
var fecha=crear_input_date("fecha1","3738");
var hora=crear_input_time("hora1","1810");
var boton=crear_input_submit("boton1","1717");
var tarea=crear_textarea("aaa","222","tarea es ugual a tarea amamamammamamammamammama");

var cuerpo=document.body;

var contenedor=crear_container();

var estilo1=div("Nombre: ",texto);
var estilo2=div("Fecha: : ",fecha);
var estilo3=div("Hora:": ",hora);
var estilo4=div("",boton);



asociar_hijo(contenedor,formulario);
asociar_hijo(estilo4,contenedor);
asociar_hijo(selec,formulario);
asociar_hijo(email,formulario);
asociar_hijo(fecha,formulario);
asociar_hijo(hora,formulario);
asociar_hijo(boton,formulario);
asociar_hijo(tarea,formulario);



asociar_hijo(formulario,cuerpo);